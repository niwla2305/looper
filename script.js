import { MDCRipple } from "@material/ripple/index";
import { MDCTextField } from "@material/textfield";
import { MDCDialog } from "@material/dialog";
const errdialog = new MDCDialog(document.querySelector(".error-dialog"));
var $ = require("jquery");
jQuery = $;


window.onload = () => {
  "use strict";

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js");
  }
};

for (let index = 0; index < $(".mdc-button").length; index++) {
  const element = $(".mdc-button")[index];
  console.log(element);
  const tes = new MDCRipple(element);
}

const textField = new MDCTextField(document.querySelector(".mdc-text-field"));

function toggleDark() {
  var darkSwitch = document.getElementById("darkSwitch");
  document.getElementById("darkmode").disabled = !darkSwitch.checked;
}

//toggleDark();

window.clear2 = function() {
  document.getElementById("calcfield").value = "";
  //document.getElementById("result").innerHTML = "0";
};

window.add_to_field = function(button) {
  var char = button.lastElementChild.innerText;
  document.getElementById("calcfield").value =
    document.getElementById("calcfield").value + char;
};

window.calc = function() {
  var calcstring = document.getElementById("calcfield").value.replace(",", ".");

  try {
    $("#history").append(
      "<li>" + calcstring + " = " + eval(calcstring) + "</li>"
    );
    document.getElementById("calcfield").value = eval(calcstring);
    document.getElementById("clearlog").disabled = false;
  } catch (error) {
    errdialog.open();
  }
};

window.clearlog = function() {
  document.getElementById("history").innerHTML = "";
  document.getElementById("clearlog").disabled = true;
};

$("#calcfield").on("keyup", function(e) {
  if (e.keyCode == 13) {
    calc();
  }
});
