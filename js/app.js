import Vue from "vue";
import _ from "lodash";
import { MDCTopAppBar } from "@material/top-app-bar";
import { MDCList } from "@material/list";
var app = new Vue({
  el: "#app",
  data: {
    recordtext: "Record",
    players: [],
    awesome: false,
    ok: true,
    componentKey: 0,
  },
  methods: {
    forceRerender() {
      this.componentKey += 1;
    },
  },
});

var recordButton, stopButton, recorder;

window.onload = function () {
  recordButton = document.getElementById("recordbtn");

  // get audio stream from user's mic
  navigator.mediaDevices
    .getUserMedia({
      audio: true,
    })
    .then(function (stream) {
      recordButton.disabled = false;
      recordButton.addEventListener("click", handlebtn);
      recorder = new MediaRecorder(stream);

      // listen to dataavailable, which gets triggered whenever we have
      // an audio blob available
      recorder.addEventListener("dataavailable", onRecordingReady);
    });
    "use strict";

    if ("serviceWorker" in navigator) {
      navigator.serviceWorker.register("./sw.js");
    }
};


function handlebtn() {
  if (app.recordtext == "Record") {
    startRecording();
  } else {
    stopRecording();
  }
}

window.mutehandle = function (player) {
  var old_player = _.clone(player, true);
  player["player"].muted = !player["player"].muted;
  window.replace_value(app.players, old_player, player);
};

function startRecording() {
  app.recordtext = "Stop";
  recorder.start();
}

function stopRecording() {
  app.recordtext = "Record";
  recorder.stop();
}

function onRecordingReady(e) {
  var audio = new Audio();
  app.players.push({ player: audio });
  audio.addEventListener(
    "ended",
    function () {
      this.currentTime = 0;
      this.play();
    },
    false
  );
  // e.data contains a blob representing the recording
  audio.src = URL.createObjectURL(e.data);
  audio.play();
}

window.replace_value = function (array, oldvalue, newvalue) {
  var index = array.indexOf(oldvalue);

  if (index !== -1) {
    array[index] = newvalue;
  }
};

Vue.prototype.mutehandle = window.mutehandle;

const list = new MDCList(document.querySelector(".mdc-list"));
const topAppBarElement = document.querySelector(".mdc-top-app-bar");
const topAppBar = new MDCTopAppBar(topAppBarElement);
